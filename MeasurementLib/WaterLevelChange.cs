﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace MeasurementLib
{
    public class WaterLevelChange
    {
        public ObjectId Id { get; set; }
        public string State { get; set; }
        public BsonDateTime Timestamp { get; set; }

        public WaterLevelChange()
        {

        }

        public WaterLevelChange(string state)
        {
            State = state;          
            Timestamp = new BsonDateTime(DateTime.Now);
        }
    }
}
