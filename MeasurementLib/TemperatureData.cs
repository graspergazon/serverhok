﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace MeasurementLib
{
    public class TemperatureData
    {
        public ObjectId Id { get; set; }
        public string SensorName { get; set; }
        public double Measurement { get; set; }
        public BsonDateTime Timestamp { get; set; }

        public TemperatureData()
        {

        }

        public TemperatureData(string sensorName, double measurement)
        {
            SensorName = sensorName;
            Measurement = measurement;
            Timestamp = new BsonDateTime(DateTime.Now);
        }

    }
}
