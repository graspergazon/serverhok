﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Dashboard.Models;

namespace Dashboard.Pages
{
    public class IndexModel : PageModel
    {
        private readonly Models.ITempRepository _repo;

        public IEnumerable<TemperatureData> Temps { get; private set; }

        public IndexModel(Models.ITempRepository repo)
        {
            _repo = repo;
        }

        //public void OnGet()
        //{

        //}

        public async Task OnGetAsync()
        {
            Temps = await _repo.GetTemperaturesOfLastHour();
        }

        public async Task<TemperatureData> GetMax(DateTime from, DateTime until)
        {
            return await _repo.GetMax(from, until);            
        }
    }
}
