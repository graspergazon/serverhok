﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Dashboard.Models
{
    public class TemperatureData
    {
       
        public ObjectId Id { get; set; }
        public string SensorName { get; set; }
        public double Measurement { get; set; }
        public BsonDateTime Timestamp { get; set; }

        public TemperatureData()
        {
            Timestamp = new BsonDateTime(0);
        }

        public TemperatureData(string sensorName, double measurement)
        {
            SensorName = sensorName;
            Measurement = measurement;
            Timestamp = new BsonDateTime(DateTime.Now);
        }

    }
}
