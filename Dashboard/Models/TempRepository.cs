﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;

namespace Dashboard.Models
{
    public class TempRepository:ITempRepository
    {
        private readonly ITempContext a_context;

        public TempRepository(ITempContext context)
        {
            a_context = context;
        }

        public async Task<IEnumerable<TemperatureData>> GetAllTemperatures()
        {
            return await a_context.Temperatures.Find(_ => true).ToListAsync();

        }

        public async Task<IEnumerable<TemperatureData>> GetTodaysTemperatures()
        {
            DateTime today = DateTime.Today;

            return await a_context.Temperatures.Find( x => x.Timestamp>=today).ToListAsync();

        }

        public async Task<IEnumerable<TemperatureData>> GetTemperaturesOfLastHour()
        {
            DateTime nowMinusOneHour = DateTime.UtcNow.AddHours(-1);

            return await a_context.Temperatures.Find(x => x.Timestamp >= nowMinusOneHour).ToListAsync();

        }

        public async Task<TemperatureData> GetMax(DateTime from, DateTime until)
        {
            List<TemperatureData>  data = await a_context.Temperatures.Find(x =>( x.Timestamp >= from && x.Timestamp <=until)).ToListAsync();

            double max = double.MinValue;
            int idx = -1;
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].Measurement > max)
                {
                    max = data[i].Measurement;
                    idx = i;
                }
            }

            if (idx < 0)
                return new TemperatureData();

            return data[idx];

        }

        public async Task<TemperatureData> GetMin(DateTime from, DateTime until)
        {
            List<TemperatureData> data = await a_context.Temperatures.Find(x => (x.Timestamp >= from && x.Timestamp <= until)).ToListAsync();


            double min = double.MinValue;
            int idx = -1;
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].Measurement < min)
                {
                    min = data[i].Measurement;
                    idx = i;
                }
            }

            if (idx < 0)
                return new TemperatureData();

            return data[idx];
        }




        //public async Task<TemperatureData> GetLatestTemperature()
        //{
        //    return await a_context.Temperatures.Find(_ => true).ToListAsync()[0];
        //}

    }
}
