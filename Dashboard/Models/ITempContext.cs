﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace Dashboard.Models
{
    public interface ITempContext
    {
        IMongoCollection<TemperatureData> Temperatures { get; }
    }
}
