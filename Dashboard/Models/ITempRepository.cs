﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Dashboard.Models
{
    public interface ITempRepository
    {
        Task<IEnumerable<TemperatureData>> GetAllTemperatures();
        Task<IEnumerable<TemperatureData>> GetTodaysTemperatures();
        Task<IEnumerable<TemperatureData>> GetTemperaturesOfLastHour();
        Task<TemperatureData> GetMax(DateTime from, DateTime until);
        Task<TemperatureData> GetMin(DateTime from, DateTime until);


        //Task<TemperatureData> GetLatestTemperature();
    }
}
