﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace Dashboard.Models
{
    public class TempContext :ITempContext
    {
        private readonly IMongoDatabase a_db;

        public TempContext(MongoDBConfig config)
        {
            var client = new MongoClient(config.ConnectionString);
            a_db = client.GetDatabase(config.Database);
        }

        public IMongoCollection<TemperatureData> Temperatures => a_db.GetCollection<TemperatureData>("Temperature");
    }

}
