﻿using MongoDB.Driver;
using System;
using System.Net;
using System.Text;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using MeasurementLib;
using Microsoft.Extensions.Configuration;

namespace VVComputeServerDB
{
    class Program
    {
        private static MqttClient a_subClient;
        private static bool a_isRunning;
        private static MongoClient a_mongoClient;
        private static IMongoDatabase a_database;
        private static IMongoCollection<TemperatureData> a_tempCollection;
        private static IMongoCollection<WaterLevelChange> a_waterLevelCollection;
        private static IConfigurationRoot a_config;
        private static string a_waterLevel;

        static void Main(string[] args)
        {
            Console.WriteLine("Starting application");

            a_config = new ConfigurationBuilder().AddJsonFile("config.json").Build();

            string serverAddr = a_config["mqttServer"];
            int port = int.Parse(a_config["mqttPort"]);

            a_subClient = new MqttClient(serverAddr, port, false, null, null, MqttSslProtocols.None);
            a_subClient.MqttMsgPublishReceived += A_subClient_MqttMsgPublishReceived;

            string clientId = Guid.NewGuid().ToString();

            Console.WriteLine($"Connecting to MQTT broker {serverAddr} on port {port}");
            a_waterLevel = "Low";

            try
            {

                a_subClient.Connect(clientId);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            if (a_subClient.IsConnected)
            {
                Console.WriteLine($"Connected");

            }

            //

            string[] topics = new string[] { a_config["mqttTopic1"], a_config["mqttTopic2"] };

            // subscribe to the topic "/home/temperature" with QoS 2 
            a_subClient.Subscribe(topics, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });

            //Console.WriteLine("Hello World!");

            //System.Threading.Thread t = new System.Threading.Thread(SendingLoop);
            //t.Start();

            ConnectToDB();

            Console.ReadLine();
            a_isRunning = false;
            a_subClient.Disconnect();
            Console.WriteLine("Ending application");

        }

        private static void ConnectToDB()
        {
            string mongoHost = a_config["mongoServer"];
            int port = int.Parse(a_config["mongoPort"]);
            string serverAddress = string.Format("mongodb://{0}:{1}", mongoHost, port);

            try
            {
                Console.WriteLine($"Connecting to database: {serverAddress}");
                a_mongoClient = new MongoClient(serverAddress);
                a_database = a_mongoClient.GetDatabase(a_config["databaseName"]);
                a_tempCollection = a_database.GetCollection<TemperatureData>("Temperature");
                a_waterLevelCollection = a_database.GetCollection<WaterLevelChange>("WaterLevel");
            }
            catch (Exception ex)
            {


                Console.WriteLine($"Could not connect to database: {ex.Message}");

            }
        }

        private static void A_subClient_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            string message = Encoding.UTF8.GetString(e.Message);

            Console.WriteLine($"Received a message: {message} on topic {e.Topic} at {DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");



            if (e.Topic.Equals(a_config["mqttTopic1"]))
            {

                double temp = -1;
                if (double.TryParse(message, out temp))
                {
                    TemperatureData data = new TemperatureData("S1", temp);

                    try
                    {
                        a_tempCollection.InsertOne(data);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Could not instert to database: {ex.Message}");
                    }
                }
            }
            else if (e.Topic.Equals(a_config["mqttTopic2"]))
            {
                if (!a_waterLevel.Equals(message))
                {
                    a_waterLevel = message;

                    WaterLevelChange levelChange = new WaterLevelChange(a_waterLevel);                   
                    try
                    {
                        a_waterLevelCollection.InsertOne(levelChange);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Could not instert to database: {ex.Message}");
                    }
                }
            }

        }

        private static void SendingLoop()
        {
            a_isRunning = true;

            MqttClient client = new MqttClient(a_config["mqttServer"], int.Parse(a_config["mqttPort"]), false, null, null, MqttSslProtocols.None);

            string clientId = Guid.NewGuid().ToString();
            client.Connect(clientId);

            Random rnd = new Random();
           


            while (a_isRunning)
            {
                

                System.Threading.Thread.Sleep(5000);

                if (client.IsConnected)
                {
                    string strValue = $"blabla {rnd.Next()}";

                    // publish a message on "/home/temperature" topic with QoS 2 
                    client.Publish(a_config["mqttTopic"], Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                    Console.WriteLine("Sending data ...");
                }
            }

            System.Threading.Thread.Sleep(1000);

            client.Disconnect();
            Console.WriteLine("Ending sending loop");

        }
    }
}
